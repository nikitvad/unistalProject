package com.softgroup.android.myapplication;

import android.app.Application;

import cn.hiroz.uninstallfeedback.FeedbackUtils;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FeedbackUtils.openUrlWhenUninstall(this, "www.baidu.com");
//        FeedbackUtils.openUrlWhenUninstallViaAppProcess(this, "www.baidu.com");
//        FeedbackUtils.openUrlWhenUninstallViaForkProcess(this, "www.baidu.com");


    }
}
